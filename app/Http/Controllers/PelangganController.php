<?php

namespace App\Http\Controllers;

use App\Models\Menu;

use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function index()
    {
        $menus = Menu::with('category')->get();
        return view('pelanggan.index')->with('menus', $menus);
    }

    public function show($id)
    {
        $menu = Menu::findOrFail($id);
        return view('pelanggan.show')->with('menu', $menu);
    }
}