<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PelangganController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/pelanggan/index', function () {
//     return view('pelanggan.index');
// });
// Route::get('/pelanggan/show', function () {
//     return view('pelanggan.show');
// });
// Route::get('/admin/dashboard', function () {
//     return view('admin.dashboard');
// });
// Route::get('/admin/kategori/index', function () {
//     return view('admin.kategori.index');
// });
// Route::get('/admin/kategori/create', function () {
//     return view('admin.kategori.create');
// });
// Route::get('/admin/kategori/create', function () {
//     return view('admin.kategori.edit');
// });
// Route::get('/admin/product/index', function () {
//     return view('admin.product.index');
// });
// Route::get('/admin/product/create', function () {
//     return view('admin.product.create');
// });
// Route::get('/admin/product/edit', function () {
//     return view('admin.product.edit');
// });


// Routes for Categories


Route::get('/products', [PelangganController::class, 'index']);
Route::get('/pelanggan/{id}', [PelangganController::class, 'show'])->name('pelanggan.show');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*------------------------------------------
--------------------------------------------
All Normal Users Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:user'])->group(function () {


    Route::get('/products', [PelangganController::class, 'index']);
    Route::get('/pelanggan/{id}', [PelangganController::class, 'show'])->name('pelanggan.show');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

/*------------------------------------------
--------------------------------------------
All Admin Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:admin'])->group(function () {

    Route::resource('categories', CategoryController::class);

    Route::get('/admin/kategori', [CategoryController::class, 'index'])->name('admin.kategori.index');

    // Routes for Menus
    Route::resource('menus', MenuController::class);


    Route::get('/admin/menu', [MenuController::class, 'index'])->name('admin.menu.index');
    Route::get('/products', [PelangganController::class, 'index']);
    Route::get('/pelanggan/{id}', [PelangganController::class, 'show'])->name('pelanggan.show');
});

/*------------------------------------------
--------------------------------------------
All Admin Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:manager'])->group(function () {

    Route::resource('categories', CategoryController::class);

    Route::get('/admin/kategori', [CategoryController::class, 'index'])->name('admin.kategori.index');

    // Routes for Menus
    Route::resource('menus', MenuController::class);


    Route::get('/admin/menu', [MenuController::class, 'index'])->name('admin.menu.index');
    Route::get('/products', [PelangganController::class, 'index']);
});
