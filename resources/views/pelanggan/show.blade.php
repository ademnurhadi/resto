@extends('template.layout')

@section('content')
<!-- Show Menu Start -->
<div class="container py-5">
    <div class="row">
        <div class="col-lg-6">
            <div class="product-item">
                <div class="position-relative bg-light overflow-hidden">
                    <img class="img-fluid w-100" src="{{ asset('storage/menu_images/' . $menu->image) }}" alt="{{ $menu->name }}">
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="product-details">
                <h1 class="mb-3">{{ $menu->name }}</h1>
                <p class="text-primary mb-2">Rp{{ $menu->price }}</p>
                <p class="text-body">{{ $menu->description }}</p>
                <div class="d-flex align-items-center">
                    <a class="btn btn-primary me-3" href="#">Add to Cart</a>
                    <a class="btn btn-outline-primary" href="/products">Back to Menu</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Show Menu End -->
@endsection