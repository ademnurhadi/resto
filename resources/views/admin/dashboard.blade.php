@extends('admin.template.layout')
@section('isi')
<!-- Admin Dashboard Start -->
<div class="container mt-4 py-5">
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Kategori</h5>
                    <p class="card-text">Total Kategori: 10</strong></p>
                    <a href="#" class="btn btn-primary">Lihat Kategori</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Menu</h5>
                    <p class="card-text">Total Menu: <strong>20</strong></p>
                    <a href="#" class="btn btn-primary">Lihat Menu</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Pelanggan</h5>
                    <p class="card-text">Total Pelanggan: <strong>100</strong></p>
                    <a href="#" class="btn btn-primary">Lihat Pelanggan</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Admin Dashboard End -->
@endsection