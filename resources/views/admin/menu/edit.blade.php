@extends('admin.template.layout')
@section('isi')
<div class="container">
    <h1>Edit Menu</h1>
    <form action="{{ route('menus.update', $menu->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama:</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $menu->name }}">
        </div>
        <div class="form-group">
            <label for="price">Harga:</label>
            <input type="number" name="price" id="price" class="form-control" value="{{ $menu->price }}">
        </div>
        <div class="form-group">
            <label for="description">Keterangan:</label>
            <textarea name="description" id="description" class="form-control">{{ $menu->description }}</textarea>
        </div>
        <div class="form-group">
            <label for="image">Gambar:</label>
            <input type="file" name="image" id="image" class="form-control-file">
        </div>
        <div class="form-group">
            <label for="category">Category:</label>
            <select name="category_id" id="category" class="form-control">
                @foreach($categories as $category)
                <option value="{{ $category->id }}" @if($category->id == $menu->category_id) selected
                    @endif>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection