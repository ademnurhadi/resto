@extends('admin.template.layout')

@section('isi')
<div class="container py-5 mt-4">
    <h1>Tambah Menu</h1>
    <form action="{{ route('menus.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group mt-2">
            <label for="name">Nama:</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-group mt-2">
            <label for="price">Harga:</label>
            <input type="number" name="price" id="price" class="form-control">
        </div>
        <div class="form-group mt-2">
            <label for="description">Keterangan:</label>
            <textarea name="description" id="description" class="form-control"></textarea>
        </div>
        <div class="form-group mt-2">
            <label for="image">Gambar:</label>
            <input type="file" name="image" id="image" class="form-control-file">
        </div>
        <div class="form-group mt-2">
            <label for="category">Kategori:</label>
            <select name="category_id" id="category" class="form-control">
                @foreach($createmenus as $createmenu)
                <option value="{{ $createmenu->id }}">{{ $createmenu->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary mt-3">Simpan</button>
    </form>
</div>
@endsection