@extends('admin.template.layout')
@section('isi')
<div class="container py-5 mt-4">
    <h1>Tambah Category</h1>
    <form action="{{ route('categories.store') }}" method="POST">
        @csrf
        <div class="form-group mt-3">
            <label for="name">Nama:</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary mt-3">Simpan</button>
    </form>
</div>
@endsection